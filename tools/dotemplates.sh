#!/bin/sh

# run this inside a directory containing copies of original SBo templates.
# Take a lot of care to check the resulting files afterwards.

set -e

sed -i -e '/^# |-/N; /^# |-/,/^# |-/d' *.SlackBuild
sed -i -r '25,$ {/^[ ]+#/d}
           25,$ {/^#/d}
           s,^([^#]+)[[:blank:]]+# .*,\1,
           s,[ \t]+$,,' *.SlackBuild
sed -i -e '$!N;/^\n$/{$q;D;};P;D;' *.SlackBuild

sed -i -e "s,<appname>,@PRGNAM@,
           s,<year>,@YEAR@,
           s,<you>,@NAME@,
           s,<where you live>,<@EMAIL@>,
           s,^\(PRGNAM=\).*,\1@PRGNAM@,
           s,^\(SRCNAM=\)appname$,\1@SRCNAM@,
           s,^\(VERSION=\${VERSION:-\)[^}]\+,\1@VERSION@,
           s,^\(BUILD=\${BUILD:-\)[^}]\+,\1@BUILD@,
           s,^\(TAG=\${TAG:-\)[^}]\+,\1@TAG@,
           s,^\(TMP=\${TMP:-\)[^}]\+,\1@TMP@,
           s,^\(OUTPUT=\${OUTPUT:-\)[^}]\+,\1@OUTPUT@,
           s,^\(tar xvf \$CWD/\$PRGNAM-\$VERSION\.\)tar\.gz,\1@ARCHIVE@,
           " *.SlackBuild

sed -i -e "s,^\(appname\):,@PRGNAM@:,
           8 s, ,  ,
           9 s,appname,@PRGNAM@,
           " slack-desc

sed -i -e 's,^\(PRGNAM=\)"[^"]\+",\1"@PRGNAM@",
           s,^\(VERSION=\)"[^"]\+",\1"@VERSION@",
           s,^\(MD5SUM=\)"[^"]\+",\1"@MD5SUM@",
           s,^\(MD5SUM_x86_64=\)"[^"]\+",\1"@MD5SUM_x86_64@",
           s,^\(MAINTAINER=\)"[^"]\+",\1"@NAME@",
           s,^\(EMAIL=\)"[^"]\+",\1"@EMAIL@",
           ' template.info
